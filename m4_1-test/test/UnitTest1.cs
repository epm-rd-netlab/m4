﻿using System;
using m4_1_test;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Class1 test = new Class1();
            string actual = test.ConvertTo2("-255.255");
            Assert.AreEqual("1100000001101111111010000010100011110101110000101000111101011100", actual);

        }
        [TestMethod]
        public void TestMethod2()
        {
            Class1 test = new Class1();
            string actual = test.ConvertTo2("255.255");
            Assert.AreEqual("0100000001101111111010000010100011110101110000101000111101011100", actual);

        }
        [TestMethod]
        public void TestMethod3()
        {
            Class1 test = new Class1();
            string actual = test.ConvertTo2("4294967295.0");
            Assert.AreEqual("0100000111101111111111111111111111111111111000000000000000000000", actual);

        }
        [TestMethod]
        public void TestMethod4()
        {
            Class1 test = new Class1();
            string actual = test.ConvertTo2("-0.0");
            Assert.AreEqual("1000000000000000000000000000000000000000000000000000000000000000", actual);
                            
        }
        [TestMethod]
        public void TestMethod5()
        {
            Class1 test = new Class1();
            string actual = test.ConvertTo2("0.0");
            Assert.AreEqual("0000000000000000000000000000000000000000000000000000000000000000", actual);

        }
    }
}
