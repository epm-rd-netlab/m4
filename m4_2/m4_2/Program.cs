﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m4_2
{
    class Program
    {

            private static int Gcd(int a, int b)
            {
                return a > 0 ? Gcd(b % a, a) : b;
            }
        private static int nod(int A, int B)
                {
                    int k = 1;
                    while ((A != 0) && (B != 0))
                    {
                        while ((A % 2 == 0) && (B % 2 == 0))
                        {
                            A /= 2;
                            B /= 2;
                            k *= 2;
                        }
                        while (A % 2 == 0) A /= 2;
                        while (B % 2 == 0) B /= 2;
                        if (A >= B) A -= B; else B -= A;
                    }
                    return B * k;
                }
        private static void Main()
            {
                int[] array = {22,11,33};
                int gcd = Gcd(array[0], array[1]);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 2; i < array.Length; i++)
                    gcd = Gcd(gcd, array[i]);
                int NOD = nod(array[0], array[1]);
                watch.Stop();
            watch.Stop();
            Console.WriteLine(gcd+" "+ watch.Elapsed.TotalMilliseconds+"ms");
            var watch2 = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < array.Length; i++)
                    NOD = nod(gcd, array[i]);
            watch2.Stop();
            Console.WriteLine(NOD + " " + watch2.Elapsed.TotalMilliseconds + "ms");
     
                Console.ReadKey();
           
        }
        }
}
