﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m4_2_test
{
    public class Class1
    {
        public  int Gcd(int a, int b)
        {
            return a > 0 ? Gcd(b % a, a) : b;
        }
        public  int nod(int A, int B)
        {
            int k = 1;
            while ((A != 0) && (B != 0))
            {
                while ((A % 2 == 0) && (B % 2 == 0))
                {
                    A /= 2;
                    B /= 2;
                    k *= 2;
                }
                while (A % 2 == 0) A /= 2;
                while (B % 2 == 0) B /= 2;
                if (A >= B) A -= B; else B -= A;
            }
            return B * k;
        }
        public  int main1(int[] array)
        {
            int gcd = Gcd(array[0], array[1]);
           
            for (int i = 2; i < array.Length; i++)
                gcd = Gcd(gcd, array[i]);
            return gcd;
           
        

        }
        public int main2(int[] array)
        {
           
            int NOD = nod(array[0], array[1]);

            var watch2 = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < array.Length; i++)
                NOD = nod(NOD, array[i]);

            return NOD;;

        }
    }
}
