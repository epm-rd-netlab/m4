﻿using System;
using m4_2_test;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1_1()
        {
            Class1 test = new Class1();
            int[] arr = new int[] { 888, 45 };
            int actual = test.main1(arr);
            Assert.AreEqual(3, actual);

        }
        [TestMethod]
        public void TestMethod2_1()
        {
            Class1 test = new Class1();
            int[] arr = new int[] { 888, 45 };
            int actual = test.main2(arr);
            Assert.AreEqual(3, actual);

        }
        [TestMethod]
        public void TestMethod1_2()
        {
            Class1 test = new Class1();
            int[] arr = new int[] { 1356, 98 };
            int actual = test.main1(arr);
            Assert.AreEqual(2, actual);
        }
        [TestMethod]
        public void TestMethod2_2()
        {
            Class1 test = new Class1();
            int[] arr = new int[] { 1356, 98 };
            int actual = test.main2(arr);
            Assert.AreEqual(2, actual);
        }
    }
}
